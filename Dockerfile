FROM ubuntu:18.04
RUN apt-get -y update
RUN apt-get -y install make autoconf wget mingw-w64 
RUN wget -q https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2020q2/gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2
#RUN mkdir /usr/armgcc
RUN tar -xvjf gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2 #-C /usr/armgcc
RUN mv ./gcc-arm-none-eabi-9-2020-q2-update /usr/armgcc
RUN echo $PATH
RUN echo "export PATH="/usr/armgcc/bin:$PATH"" >> .bashrc
RUN echo $PATH


#Windows related implementation

#FROM cloudfoundry/windows2016fs
#FROM microsoft/windowsservercore
#FROM microsoft/nanoserver:1809
#FROM python:3-windowsservercore
#FROM portainerci/windows-nanoserver
#FROM ldti/nanoserver-python
#FROM hub.docker.com/pthomson/nanoserver

#ADD https://downloads.sourceforge.net/project/gnuwin32/make/3.81/make-3.81-bin.zip?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fgnuwin32%2Ffiles%2Fmake%2F3.81%2Fmake-3.81-bin.zip%2Fdownload&ts=1592145108 c:/make-3.81-bin.zip
#ADD https://downloads.sourceforge.net/project/gnuwin32/make/3.81/make-3.81-bin.zip?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fgnuwin32%2Ffiles%2Fmake%2F3.81%2Fmake-3.81-bin.zip%2Fdownload%3Fuse_mirror%3Dexcellmedia%26download%3D&ts=1592145450&use_mirror=netcologne c:/make1.zip
#ADD https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2020q2/gcc-arm-none-eabi-9-2020-q2-update-win32.zip c:/gcc-arm-none-eabi-9-2020-q2-update-win32.zip

#SHELL ["powershell","-command"]
#RUN Expand-Archive -Path c:/make-3.81-bin.zip -DestinationPath c:/make # the subfolder bin has make.exe
#RUN Expand-Archive -Path c:/make1.zip -DestinationPath c:/make1 # the subfolder bin has make.exe
#RUN Expand-Archive -Path c:/gcc-arm-none-eabi-9-2020-q2-update-win32.zip -DestinationPath c:/armgcc #the subfolder bin has all the executables starting with arm-none-eabi-

#SHELL ["cmd", "/S", "/C"]
#RUN setx /M PATH "%PATH%;C:\armgcc\bin;C:\make\bin"




